# Samba Audit Parser

Samba audit log parse to remove excess white space and un-needed from pipe delimited file.

Installation:
```
pip install -r requirements.txt
```

CLI Usage:
===============================================================================

```
smb_audit_log_tool.py --ignore-gooey --help
usage: smb_audit_log_tool.exe [-h] {Parsing,Summary,Search} ...
This tool will search a parsed .ftr file from the audit_parser tool.
positional arguments:
  {Parsing,Summary,Search} commands
optional arguments:
  -h, --help            show this help message and exit
```


==============================================================================

```
smb_audit_log_tool.py --ignore-gooey Parsing --help
usage: smb_audit_log_tool.exe Parsing [-h] -sd SOURCE_DIRECTORY -of OUTPUT_FILE [-c]
optional arguments:
  -h, --help            show this help message and exit
  -sd SOURCE_DIRECTORY, --source-directory SOURCE_DIRECTORY
                        Source log directory contianing ONLY audit log files (Format: share/folder1/folder2/folderN
  -of OUTPUT_FILE, --output-file OUTPUT_FILE
                        Name of the output file(Format: customer_location.ftr)
  -c, --csv             Output a CSV file in addition to other options
```
===============================================================================  
```
smb_audit_log_tool.py --ignore-gooey Summary --help
usage: smb_audit_log_tool.exe Summary [-h] -if FTR_FILE
optional arguments:
  -h, --help            show this help message and exit
  -if FTR_FILE, --input-file FTR_FILE
                        Name of the input file(Format: customer_location.ftr)
```
===============================================================================
```
smb_audit_log_tool.py --ignore-gooey Search --help
usage: smb_audit_log_tool.exe Search [-h] -if FTR_FILE -ss SEARCH_STRING -sf {path,share,user} [--ACEChanged]
                                     [--ACLAdded] [--ACLDeleted] [--AclDenied] [--chown] [--create] [--createDenied]
                                     [--delete] [--deleteDenied] [--move] [--open] [--OpenDenied] [--setattrib]
                                     [--setdacl] [--write]
optional arguments:
  -h, --help            show this help message and exit
  -if FTR_FILE, --input-file FTR_FILE
                        The parsed FTR audit log file.
  -ss SEARCH_STRING, --search-string SEARCH_STRING
                        String to search for in the specified field.
  -sf {path,share,user}, --search-field {path,share,user}
                        Field to be searched from the audit logs: path, user, result, share.
Show SMB Operations:
  --ACEChanged          ACEChanged
  --ACLAdded            ACLAdded
  --ACLDeleted          ACLDeleted
  --AclDenied           AclDenied
  --chown               chown
  --create              create
  --createDenied        createDenied
  --delete              delete
  --deleteDenied        deleteDenied
  --move                move
  --open                open
  --OpenDenied          OpenDenied
  --setattrib           setattrib
  --setdacl             setdacl
  --write               write
