import sys
import os
import pandas as pd
from pandas.api.types import CategoricalDtype
import matplotlib.pyplot as plt
from datetime import datetime
import feather
import logging
import math
from gooey import Gooey, GooeyParser
import re

def convert_size(size_bytes):
   if size_bytes == 0:
       return "0B"
   size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
   i = int(math.floor(math.log(size_bytes, 1024)))
   p = math.pow(1024, i)
   s = round(size_bytes / p, 2)
   return "%s %s" % (s, size_name[i])

def extract_epoch(seq):
    seq_type= type(seq)
    return int(seq_type().join(filter(seq_type.isdigit, seq)))

def convert_time(epoch_time):
    return datetime.fromtimestamp(extract_epoch(epoch_time))

def show_file_details(ftr_file, df):
    logging.info('Most recent time of dataset: ' + str(df.local_time.max()))
    logging.info('Oldest time of dataset: ' + str(df.local_time.min()))
    logging.info('Time between start and end of dataset: ' + str(df.local_time.max() - df.local_time.min()))
    logging.info("=====================================================") 
    

def show_summary(ftr_file, df, time_interval):   
    logging.info('Totals per SMB operation type for this dataset:\n' + str(df['smb_operation_type'].value_counts()))
    logging.info("=====================================================")
    logging.info('Top 10 users for this dataset:\n' + str(df['user'].value_counts().nlargest(10)))
    logging.info("=====================================================")
    logging.info('Top 10 shares for this dataset:\n' + str(df['share'].value_counts().nlargest(10)))
    logging.info("=====================================================")
    logging.info('Top 10 paths for this dataset:\n' + str(df['path'].value_counts().nlargest(10)))
    logging.info("=====================================================")
    #logging.info(str(df.groupby([df.local_time.dt.floor(time_interval), 'smb_operation_type']).size()))
    
    plt.rc('legend',fontsize=6)
    #df.groupby([df.local_time.dt.floor('60min'), 'smb_operation_type']).size().plot()
    #df.groupby([df.local_time.dt.floor(time_interval), 'smb_operation_type']).size().unstack().plot(colormap='nipy_spectral').legend(loc='center left',bbox_to_anchor=(1.0, 0.5))
    df.groupby([df.local_time.dt.floor(time_interval), 'smb_operation_type']).size().unstack().plot(colormap='nipy_spectral', x_compat=True).legend(loc='best')
    #df.groupby([df.local_time.dt.floor(time_interval), 'smb_operation_type']).size().unstack().plot(colormap='nipy_spectral', x_compat=True).legend(loc='best')
    plt.show()



@Gooey(
    program_name="SMB Audit Search Tool",
    program_description="Allows for quick summary and string search of SMB audit log files",
    default_size=(1024, 720),
    navigation="SIDEBAR",
    show_sidebar=True
)
def main():
    try:
        date = datetime.now().strftime("%Y_%m_%d-%I_%M_%S_%p")
        log_file = "samba_audit_log_tool." + date + ".log"
        logging.basicConfig(
            level=logging.INFO,
            format="%(asctime)s [%(levelname)s] %(message)s",
            handlers=[
                logging.FileHandler(log_file, 'w', 'utf-8'),
                logging.StreamHandler()
            ]
        )  
        
        smb_operations = ["op=ACEChanged", "op=ACLAdded", "op=ACLDeleted", "op=AclDenied", "op=chown",
                          "op=create", "op=createDenied", "op=delete", "op=deleteDenied", "op=move",
                          "op=open", "op=OpenDenied", "op=setattrib", "op=setdacl", "op=write"]
        smb_operation_type = CategoricalDtype(categories=smb_operations)
        
        parser = GooeyParser(description='This tool will search a parsed .ftr file from the audit_parser tool.')
        subparsers = parser.add_subparsers(help='commands', dest='command')
        subparser_1 = subparsers.add_parser('Parsing')
        subparser_1.add_argument('-sd', '--source-directory', action='store', dest='source_directory', widget='DirChooser', required=True, type=str, help='Source log directory contianing ONLY audit log files (Format: share/folder1/folder2/folderN')
        subparser_1.add_argument('-of', '--output-file', action='store', dest='output_file', required=True, type=str, help='Label for output files')
        subparser_1.add_argument('-c', '--csv', action='store_true', dest='make_csv', default=False, help='Output a CSV file in addition to other options')
        subparser_1.add_argument('--debug', action='store_true', dest='is_debug', default=False, help='Enables debug output of arguments')
        
        subparser_2 = subparsers.add_parser('Summary')
        subparser_2.add_argument('-if', '--input-file', action='store', dest='ftr_file', type=str, widget="FileChooser", required=True, help='Name of the input file(Format: customer_location.ftr)')
        subparser_2.add_argument('-ti', '--time-interval', action='store', dest='time_interval', type=str, widget="Dropdown", choices=['5min', '10min', '15min', '20min', '30min', '60min'], required=True, default='30min', help='Time interval used for summary computations')  
        subparser_2.add_argument('--debug', action='store_true', dest='is_debug', default=False, help='Enables debug output of arguments')
        
        subparser_3 = subparsers.add_parser('Search')    
        subparser_3.add_argument('-if', '--input-file', action='store', dest='ftr_file', type=str, widget="FileChooser", required=True, help='The parsed FTR audit log file.')     
        subparser_3.add_argument('-ss', '--search-string', action='store', dest='search_string', type=str, required=True, help='String to search for in the specified field.')
        subparser_3.add_argument('-sf', '--search-field', action='store', dest='search_field', type=str, widget="Dropdown", choices=['path', 'share', 'user'], required=True, default='path', help='Field to be searched from the audit logs: path, user, result, share.')  
        subparser_3.add_argument('--debug', action='store_true', dest='is_debug', default=False, help='Enables debug output of arguments')
        
        
        smb_ops_group = subparser_3.add_argument_group('Show SMB Operations')
        for op in smb_operations:
            op_string = re.sub('op=', '', op)
            op_command = "--" + op_string
            smb_ops_group.add_argument(op_command, action='store_false', default=True, widget='CheckBox', help=op_string )
               
        arguments = parser.parse_args()
        pd.set_option('display.max_rows', None)

        if (arguments.is_debug):
            logging.info("Usage:\n{0}\n".format(" ".join([x for x in sys.argv])))
            logging.info("")
            logging.info("All settings used:")
            for k,v in sorted(vars(arguments).items()):
                logging.info("{0}: {1}".format(k,v))
        
        if (arguments.command == "Parsing"):
            import glob
            col_names = ["col0", "col1", "col2", "col3", "result", "col5", "user", "col7", "smb_operation_type", "utc_time", "local_time", "col11", "share", "path", "col14", "col15", "col16", "col17"]
            small_dfs = [] 

            all_files = glob.glob(os.path.join(arguments.source_directory, "audit.*.log"))
            total_size = 0

            for file in all_files:
                total_size += os.path.getsize(file)
            logging.info(all_files)
            df_from_each_file = (pd.read_csv(f, sep='|', names=col_names, index_col=False, low_memory=False, parse_dates=['local_time'], date_parser=convert_time, usecols=[4, 6, 8, 10, 12, 13]) for f in all_files)

            concatenated_df  = pd.concat(df_from_each_file, copy=False)
            #concatenated_df.info()
            #logging.info(concatenated_df.memory_usage(deep=True) / 1e6)

            concatenated_df.smb_operation_type  = concatenated_df.smb_operation_type.astype('category')
            concatenated_df.info()
            logging.info(concatenated_df.memory_usage(deep=True) / 1e6)


            logging.info('Creating feather file at: ' + str(arguments.output_file) + ".ftr")
            concatenated_df.reset_index().to_feather(arguments.output_file + ".ftr")
            if (arguments.make_csv):
                logging.info('Creating output file at: ' + str(arguments.output_file) + ".csv")
                concatenated_df.reset_index().to_csv(arguments.output_file + ".csv")
                
            ftr_size = os.path.getsize(arguments.output_file + ".csv")
            logging.info("Completed parsing " + str(convert_size(total_size)) + " to " + str(convert_size(ftr_size)))
            logging.info("=====================================================")
            logging.info('Most recent time of dataset: ' + str(concatenated_df.local_time.max()))
            logging.info('Oldest time of dataset: ' + str(concatenated_df.local_time.min()))
            logging.info('Time between start and end of dataset: ' + str(concatenated_df.local_time.max() - concatenated_df.local_time.min()))
            logging.info("=====================================================")     
        
        if (arguments.command == "Summary"):
            df = pd.read_feather(arguments.ftr_file)
            logging.info('Loading FTR file at: ' + str(arguments.ftr_file))
            logging.info("=====================================================")
            show_file_details(arguments.ftr_file, df)
            show_summary(arguments.ftr_file, df, arguments.time_interval)

        if (arguments.command == "Search"): 
            df = pd.read_feather(arguments.ftr_file)
            logging.info('Loading FTR file at: ' + str(arguments.ftr_file))
            logging.info("=====================================================")
            show_file_details(arguments.ftr_file, df)
            
            show_smb_ops = []
            if (not arguments.ACEChanged): 
                show_smb_ops.append('op=ACEChanged')

            if (not arguments.ACLAdded):
                show_smb_ops.append('op=ACLAdded')

            if (not arguments.ACLDeleted):
                show_smb_ops.append('op=ACLDeleted')
            
            if (not arguments.AclDenied):
                show_smb_ops.append('op=AclDenied')
            
            if (not arguments.chown):
                show_smb_ops.append('op=chown')
            
            if (not arguments.create):
                show_smb_ops.append('op=create')
            
            if (not arguments.createDenied):
                show_smb_ops.append('op=createDenied')

            if (not arguments.delete):
                show_smb_ops.append('op=delete')

            if (not arguments.deleteDenied):
                show_smb_ops.append('op=deleteDenied')

            if (not arguments.move):
                show_smb_ops.append('op=move')

            if (not arguments.open):
                show_smb_ops.append('op=open')

            if (not arguments.OpenDenied):
                show_smb_ops.append('op=OpenDenied')

            if (not arguments.setattrib):
                show_smb_ops.append('op=setattrib')

            if (not arguments.setdacl):
                show_smb_ops.append('op=setdacl')

            if (not arguments.write):
                show_smb_ops.append('op=write')

            #logging.info(df[df[arguments.search_field].str.contains(arguments.search_string)].to_string())
            search_results = df[(df[arguments.search_field].str.contains(arguments.search_string)) & (df['smb_operation_type'].isin(show_smb_ops))].to_string()
            logging.info(search_results)
            #logging.info(df[(df[arguments.search_field].str.contains(arguments.search_string)) & (df['smb_operation_type'].isin(show_smb_ops))].to_string())

    except KeyboardInterrupt:
        logging.getLogger().fatal('Cancelled by user.')    


if __name__ == '__main__':
    main()
